<?php

namespace Drupal\drupalorg_migrate\Plugin\migrate\source;

use Drupal\file\Plugin\migrate\source\d7\File;

/**
 * Drupal 7 project images source from database.
 *
 * @MigrateSource(
 *   id = "d7_file_project_images",
 *   source_module = "file"
 * )
 */
class ProjectImageFile extends File {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = parent::query();
    $or_condition = $query->orConditionGroup()
      ->condition('f.uri', 'public://project-images/%', 'LIKE')
      ->condition('f.uri', 'public://images/%', 'LIKE');
    $query->condition($or_condition);

    $alt_alias = $query->addJoin('left', 'field_data_field_project_images', 'alt', 'f.fid = %alias.field_project_images_fid');
    $query->addField($alt_alias, 'field_project_images_alt', 'alt');
    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = parent::fields();
    $fields['alt'] = $this->t('Alt text of the file (if present)');
    return $fields;
  }

}
