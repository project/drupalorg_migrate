<?php

namespace Drupal\drupalorg_migrate\Plugin\migrate\source;

use Drupal\file\Plugin\migrate\source\d7\File;

/**
 * Drupal 7 User pictures source from database.
 *
 * @MigrateSource(
 *   id = "d7_file_user_pictures",
 *   source_module = "file"
 * )
 */
class UserPictureFile extends File {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = parent::query();
    $query->addJoin('INNER', 'users', 'u', 'f.fid = %alias.picture');

    return $query;
  }

}
