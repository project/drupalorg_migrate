## Setting charset of the migration database

To make sure that the migration database has the right charset, see the below.

Set char set:
```
ALTER DATABASE migrate CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci;
```

Command to generate commands to change char set on all tables:
```
SELECT CONCAT('ALTER TABLE `', TABLE_NAME,'` CONVERT TO CHARACTER SET utf8_general_ci COLLATE utf8mb4_general_ci;') AS mySQL
FROM INFORMATION_SCHEMA.TABLES
WHERE TABLE_SCHEMA= "migrate"
AND TABLE_TYPE="BASE TABLE"
```

Generated commands (truncated, just grab the output from the above instead):
```
 ALTER TABLE `access` CONVERT TO CHARACTER SET utf8_general_ci COLLATE utf8mb4_general_ci;
 ALTER TABLE `actions` CONVERT TO CHARACTER SET utf8_general_ci COLLATE utf8mb4_general_ci;
 ALTER TABLE `advagg_aggregates` CONVERT TO CHARACTER SET utf8_general_ci COLLATE utf8mb4_general_ci;
 ALTER TABLE `advagg_aggregates_hashes` CONVERT TO CHARACTER SET utf8_general_ci COLLATE utf8mb4_general_ci;
 ...
 ALTER TABLE `versioncontrol_project_maintainers_block_cache` CONVERT TO CHARACTER SET utf8_general_ci COLLATE utf8mb4_general_ci;
 ALTER TABLE `versioncontrol_project_projects` CONVERT TO CHARACTER SET utf8_general_ci COLLATE utf8mb4_general_ci;
 ALTER TABLE `versioncontrol_release_labels` CONVERT TO CHARACTER SET utf8_general_ci COLLATE utf8mb4_general_ci;
 ALTER TABLE `versioncontrol_repositories` CONVERT TO CHARACTER SET utf8_general_ci COLLATE utf8mb4_general_ci;
 ALTER TABLE `versioncontrol_sync_log` CONVERT TO CHARACTER SET utf8_general_ci COLLATE utf8mb4_general_ci;
 ALTER TABLE `versioncontrol_views_sets` CONVERT TO CHARACTER SET utf8_general_ci COLLATE utf8mb4_general_ci;
 ALTER TABLE `views_content_cache` CONVERT TO CHARACTER SET utf8_general_ci COLLATE utf8mb4_general_ci;
 ALTER TABLE `views_display` CONVERT TO CHARACTER SET utf8_general_ci COLLATE utf8mb4_general_ci;
 ALTER TABLE `views_object_cache` CONVERT TO CHARACTER SET utf8_general_ci COLLATE utf8mb4_general_ci;
 ALTER TABLE `views_view` CONVERT TO CHARACTER SET utf8_general_ci COLLATE utf8mb4_general_ci;
 ALTER TABLE `watchdog` CONVERT TO CHARACTER SET utf8_general_ci COLLATE utf8mb4_general_ci;
```
