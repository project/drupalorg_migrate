<?php

namespace Drupal\drupalorg_migrate\Plugin\migrate\source;

use Drupal\file\Plugin\migrate\source\d7\File;

/**
 * Drupal 7 blog post images source from database.
 *
 * @MigrateSource(
 *   id = "d7_file_blog_post_files",
 *   source_module = "file"
 * )
 */
class BlogPostFile extends File {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = parent::query();
    $query->addJoin('INNER', 'field_data_upload', 'fdu', 'f.fid = %alias.upload_fid');
    $query->condition('fdu.entity_type', 'node');
    $query->condition('fdu.bundle', 'post');

    return $query;
  }

}
