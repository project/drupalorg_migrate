<?php

namespace Drupal\drupalorg_migrate\Plugin\migrate\source;

use Drupal\file\Plugin\migrate\source\d7\File;

/**
 * Drupal 7 Contributor images source from database.
 *
 * @MigrateSource(
 *   id = "d7_file_release_files",
 *   source_module = "file"
 * )
 */
class ReleaseFilesFile extends File {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = parent::query();
    $query->addJoin('INNER', 'field_data_field_release_file', 'frf', 'f.fid = %alias.field_release_file_fid');
    $query->condition('frf.entity_type', 'field_collection_item');
    $query->condition('frf.bundle', 'field_release_files');

    return $query;
  }

}
