#!/bin/bash

TYPE="${1:-all}"

if [[ "$TYPE" == "all" || "$TYPE" == "taxonomies" ]]; then
  # Taxonomies.
  drush migrate:import drupalorg_migrate_forums --update
  drush migrate:import drupalorg_migrate_module_categories --update
  drush migrate:import drupalorg_migrate_drupal_version --update
  drush migrate:import drupalorg_migrate_core_compatibility --update
  drush migrate:import drupalorg_migrate_release_type --update
  drush migrate:import drupalorg_migrate_audience --update
  drush migrate:import drupalorg_migrate_maintenance_status --update
  drush migrate:import drupalorg_migrate_development_status --update
  drush migrate:import drupalorg_migrate_services --update
  drush migrate:import drupalorg_migrate_sectors --update
  drush migrate:import drupalorg_migrate_locations --update
  drush migrate:import drupalorg_migrate_keywords --update
  drush migrate:import drupalorg_migrate_level --update
  drush migrate:import drupalorg_migrate_book_availability --update
  drush migrate:import drupalorg_migrate_book_format --update
  drush migrate:import drupalorg_migrate_areas_of_expertise --update
  drush migrate:import drupalorg_migrate_organization_type --update
  drush migrate:import drupalorg_migrate_tags --update
  drush migrate:import drupalorg_migrate_contribution_area --update
  drush migrate:import drupalorg_migrate_hosting_features --update
fi

if [[ "$TYPE" == "all" || "$TYPE" == "users" ]]; then
  drush migrate:import drupalorg_migrate_user_roles
  drush migrate:import drupalorg_migrate_contributor_skill --feedback 10000
  drush migrate:import drupalorg_migrate_contributor_role --feedback 10000
  # @todo Redo these migrations.
  # drush migrate:import drupalorg_migrate_field_collection_organizations --feedback 10000
  # drush migrate:import drupalorg_migrate_field_collection_contribution_role --feedback 10000
  drush migrate:import drupalorg_migrate_users --feedback 1000
fi

if [[ "$TYPE" == "full-users" ]]; then
  drush migrate:import drupalorg_migrate_user_roles --update
  drush migrate:import drupalorg_migrate_contributor_skill --feedback 10000
  drush migrate:import drupalorg_migrate_contributor_role --feedback 10000
  # @todo Redo these migrations.
  # drush migrate:import drupalorg_migrate_field_collection_organizations --feedback 10000
  # drush migrate:import drupalorg_migrate_field_collection_contribution_role --feedback 10000
  drush state:set drupalorg_migrate.drupalorg_migrate_users_last_record 0 --input-format=integer
  drush migrate:import drupalorg_migrate_users --feedback 10000
fi

if [[ "$TYPE" == "all" || "$TYPE" == "files" ]]; then
  # Files
  drush migrate:import drupalorg_migrate_project_files --feedback 10000
  drush migrate:import drupalorg_migrate_blog_post_files --feedback 10000
  drush migrate:import drupalorg_migrate_blog_post_media_documents --feedback 10000
  drush migrate:import drupalorg_migrate_blog_post_media_images --feedback 10000
  drush migrate:import drupalorg_migrate_organization_files --feedback 10000
  drush migrate:import drupalorg_migrate_organization_media_images --feedback 10000
  drush migrate:import drupalorg_migrate_casestudy_cover_photo_files --feedback 10000
  drush migrate:import drupalorg_migrate_casestudy_cover_photo_media --feedback 10000
  drush migrate:import drupalorg_migrate_casestudy_images_files --feedback 10000
  drush migrate:import drupalorg_migrate_casestudy_images_media --feedback 10000
  drush migrate:import drupalorg_migrate_casestudy_logo_files --feedback 10000
  drush migrate:import drupalorg_migrate_casestudy_logo_media --feedback 10000
  drush migrate:import drupalorg_migrate_casestudy_mainimage_files --feedback 10000
  drush migrate:import drupalorg_migrate_casestudy_mainimage_media --feedback 10000
  drush migrate:import drupalorg_migrate_casestudy_upload_files --feedback 10000
  drush migrate:import drupalorg_migrate_casestudy_upload_media_documents --feedback 10000
  drush migrate:import drupalorg_migrate_casestudy_upload_media_images --feedback 10000
  drush migrate:import drupalorg_migrate_documentation_files --feedback 10000
  drush migrate:import drupalorg_migrate_documentation_media_images --feedback 10000
  drush migrate:import drupalorg_migrate_documentation_media_documents --feedback 10000
  drush migrate:import drupalorg_migrate_guide_files --feedback 10000
  drush migrate:import drupalorg_migrate_guide_media_images --feedback 10000
  drush migrate:import drupalorg_migrate_guide_media_documents --feedback 10000
  drush migrate:import drupalorg_migrate_forum_upload_files --feedback 10000
  drush migrate:import drupalorg_migrate_forum_upload_media_documents --feedback 10000
  drush migrate:import drupalorg_migrate_forum_upload_media_images --feedback 10000
  drush migrate:import drupalorg_migrate_event_cover_photo_files --feedback 10000
  drush migrate:import drupalorg_migrate_event_cover_photo_media --feedback 10000
  drush migrate:import drupalorg_migrate_event_event_logo_files --feedback 10000
  drush migrate:import drupalorg_migrate_event_event_logo_media --feedback 10000
  drush migrate:import drupalorg_migrate_user_picture_files --feedback 10000
  drush migrate:import drupalorg_migrate_changenotice_upload_files --feedback 10000
  drush migrate:import drupalorg_migrate_changenotice_upload_media_documents --feedback 10000
  drush migrate:import drupalorg_migrate_changenotice_upload_media_images --feedback 10000
  drush migrate:import drupalorg_migrate_contributor_task_upload_files --feedback 10000
  drush migrate:import drupalorg_migrate_contributor_task_upload_media_documents --feedback 10000
  drush migrate:import drupalorg_migrate_contributor_task_upload_media_images --feedback 10000
  drush migrate:import drupalorg_migrate_book_listing_cover_image_files --feedback 10000
  drush migrate:import drupalorg_migrate_book_listing_cover_image_media --feedback 10000
  drush migrate:import drupalorg_migrate_logo_logo_image_files --feedback 10000
  drush migrate:import drupalorg_migrate_logo_logo_image_media --feedback 10000
  drush migrate:import drupalorg_migrate_hosting_listing_logo_image_files --feedback 10000
  drush migrate:import drupalorg_migrate_hosting_listing_logo_image_media --feedback 10000
  drush migrate:import drupalorg_migrate_page_upload_files --feedback 10000
  drush migrate:import drupalorg_migrate_page_upload_media_documents --feedback 10000
  drush migrate:import drupalorg_migrate_page_upload_media_images --feedback 10000
fi

if [[ "$TYPE" == "all" || "$TYPE" == "project-browser" ]]; then
  # Content & Paragraphs.
  # @todo organizations is in the "content" section. maybe it can be removed from here.
  drush migrate:import drupalorg_migrate_organizations
  # @todo Redo this migration.
  # drush migrate:import drupalorg_migrate_field_collection_supporting_organizations
  drush migrate:import drupalorg_migrate_project_module
  drush migrate:import drupalorg_migrate_project_core
  drush migrate:import drupalorg_migrate_project_distribution
  drush migrate:import drupalorg_migrate_project_drupalorg
  drush migrate:import drupalorg_migrate_project_theme
  drush migrate:import drupalorg_migrate_project_theme_engine
  drush migrate:import drupalorg_migrate_project_translation
  drush migrate:import drupalorg_migrate_project_general
  # Maintainers of the projects.
  # drush migrate:import drupalorg_migrate_project_maintainers
  # Mapping of projects to GitLab repos.
  drush migrate:import drupalorg_migrate_project_repositories
fi

if [[ "$TYPE" == "all" || "$TYPE" == "content" ]]; then
  # Generic content migrations.
  drush migrate:import drupalorg_migrate_post --feedback 10000
  drush migrate:import drupalorg_migrate_organizations --update --feedback 10000
  drush migrate:import drupalorg_migrate_casestudy --feedback 10000
  drush migrate:import drupalorg_migrate_page --feedback 10000
  drush migrate:import drupalorg_migrate_documentation --feedback 10000
  drush migrate:import drupalorg_migrate_guide --feedback 10000
  drush migrate:import drupalorg_migrate_event --feedback 10000
  drush migrate:import drupalorg_migrate_changenotice --feedback 10000
  drush migrate:import drupalorg_migrate_contributor_task --feedback 10000
  drush migrate:import drupalorg_migrate_security_advisory --feedback 10000
  drush migrate:import drupalorg_migrate_book_listing --feedback 10000
  drush migrate:import drupalorg_migrate_logo --feedback 10000
  drush migrate:import drupalorg_migrate_hosting_listing --feedback 10000
  # Forums and comments.
  drush migrate:import drupalorg_migrate_forum --feedback 10000
  drush migrate:import drupalorg_migrate_forum_comment --feedback 10000

fi

if [[ "$TYPE" == "all" || "$TYPE" == "usage" ]]; then
  # Project usage data
  # @todo Redo this migration.
  # drush migrate:import drupalorg_migrate_field_collection_release_files
  drush migrate:import drupalorg_migrate_project_release

  ## Needed to truncate the table locally due to memory limits. Just the delete query took minutes to run.
  ## DELETE FROM project_usage_week_release WHERE timestamp < 1633219200; -- October 2021
  drush migrate:import drupalorg_migrate_project_usage_week_release
fi
