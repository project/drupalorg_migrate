<?php

namespace Drupal\drupalorg_migrate\Plugin\migrate\source;

use Drupal\file\Plugin\migrate\source\d7\File;

/**
 * Drupal 7 Logo images source from database.
 *
 * @MigrateSource(
 *   id = "d7_file_hosting_listing_logo_files",
 *   source_module = "file"
 * )
 */
class HostingListingLogoFile extends File {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = parent::query();
    $query->addJoin('INNER', 'field_data_field_logo', 'fl', 'f.fid = %alias.field_logo_fid');
    $query->condition('fl.entity_type', 'node');
    $query->condition('fl.bundle', 'hosting_listing');

    return $query;
  }

}
