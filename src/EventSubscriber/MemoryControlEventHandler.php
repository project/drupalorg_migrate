<?php

namespace Drupal\drupalorg_migrate\EventSubscriber;

use Drupal\Core\Cache\MemoryCache\MemoryCacheInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\migrate\Event\MigrateEvents;
use Drupal\migrate\Event\MigratePostRowSaveEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Migration event handler for migrations.
 */
class MemoryControlEventHandler implements EventSubscriberInterface {

  /**
   * Keeps track of how many iterations will run until memory cache is flushed.
   */
  protected int $iterationCountUntilCacheFlush = 1000;

  public function __construct(
    protected ConfigFactoryInterface $configFactory,
    protected MemoryCacheInterface $memoryCache,
    protected EntityTypeManagerInterface $entityTypeManager
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events = [];
    $events[MigrateEvents::POST_ROW_SAVE][] = ['reduceMemoryUsage'];
    return $events;
  }

  /**
   * Clears memory caches after a limit of items imported to reduce leakage.
   *
   * @param \Drupal\migrate\Event\MigratePostRowSaveEvent $event
   *   The migrate event object.
   */
  public function reduceMemoryUsage(MigratePostRowSaveEvent $event): void {
    $this->iterationCountUntilCacheFlush--;
    if ($this->iterationCountUntilCacheFlush <= 0) {
      // Temp debug.
      dump(round(memory_get_usage()/1048576,2). ' MB');
      // $this->memoryCache->deleteAll();
      // $this->configFactory->clearStaticCache();
      // // Entity storage can blow up with caches so clear them out.
      // foreach ($this->entityTypeManager->getDefinitions() as $id => $definition) {
      //   $this->entityTypeManager->getStorage($id)->resetCache();
      // }
      $this->iterationCountUntilCacheFlush = 1000;
    }
  }

}
