<?php

namespace Drupal\drupalorg_migrate\Plugin\migrate\source;

use Drupal\file\Plugin\migrate\source\d7\File;

/**
 * Drupal 7 Event images source from database.
 *
 * @MigrateSource(
 *   id = "d7_file_event_files",
 *   source_module = "file"
 * )
 */
class EventFile extends File {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = parent::query();
    // Filter by case study image field.
    if (!isset($this->configuration['source_field'])) {
      $query->addJoin('INNER', 'field_data_field_event_logo', 'fel', 'f.fid = %alias.field_event_logo_fid');
      $query->condition('fel.entity_type', 'node');
      $query->condition('fel.bundle', 'event');
    }
    elseif ($this->configuration['source_field'] == 'cover_photo') {
      $query->addJoin('INNER', 'field_data_field_cover_photo', 'fcp', 'f.fid = %alias.field_cover_photo_fid');
      $query->condition('fcp.entity_type', 'node');
      $query->condition('fcp.bundle', 'event');
    }

    return $query;
  }

}
