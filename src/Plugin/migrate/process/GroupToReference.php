<?php

namespace Drupal\drupalorg_migrate\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Organic group reference to entity reference field.
 *
 * @MigrateProcessPlugin(
 *   id = "og_entity_ref"
 * )
 *
 * Look up OG group from current entity id and set it as an entity reference:
 *
 * @code
 * og_group_ref:
 *   plugin: og_entity_ref
 *   source: eid
 *   og_source_field: og_group_ref
 * @endcode
 *
 */
class GroupToReference extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    //Use the field name and the current entity id to get the group parent.
    $og_source_field = $this->configuration['og_source_field'];
    $db = \Drupal\Core\Database\Database::getConnection('default', 'migrate');
    $query = $db->select('og_membership', 'ogm');
    $query->condition('ogm.etid', $value);
    $query->condition('ogm.field_name', $og_source_field);
    $query->addField('ogm', 'gid');
    $result = $query->execute();
    $record = $result->fetchAssoc();

    if (!$record) {
      $this->stopPipeline();
      return NULL;
    }

    return $record['gid'];
  }
}
