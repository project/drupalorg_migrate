<?php

namespace Drupal\drupalorg_migrate\Plugin\migrate\source;

use Drupal\file\Plugin\migrate\source\d7\File;

/**
 * Drupal 7 organization images source from database.
 *
 * @MigrateSource(
 *   id = "d7_file_organization_files",
 *   source_module = "file"
 * )
 */
class OrganizationLogoFile extends File {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = parent::query();
    $query->addJoin('INNER', 'field_data_field_logo', 'fl', 'f.fid = %alias.field_logo_fid');
    $query->condition('fl.entity_type', 'node');
    $query->condition('fl.bundle', 'organization');

    return $query;
  }

}
