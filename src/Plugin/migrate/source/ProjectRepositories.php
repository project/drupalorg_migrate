<?php

namespace Drupal\drupalorg_migrate\Plugin\migrate\source;

use Drupal\migrate\Row;
use Drupal\migrate_plus\Plugin\migrate\source\Table;

/**
 * Drupal 7 project repositories information coming from different tables.
 *
 * @MigrateSource(
 *   id = "d7_project_repositories",
 *   source_module = "migrate_plus"
 * )
 */
class ProjectRepositories extends Table {

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $nid = $row->getSourceProperty('nid');
    $repo_id = $row->getSourceProperty('repo_id');

    // Get the rest of the properties that we want for the destination table.
    $query = $this->select('versioncontrol_project_projects', 'vpp')
      ->fields('vpp', ['nid', 'repo_id'])
      ->condition('vpp.nid', $nid)
      ->condition('vpp.repo_id', $repo_id);
    $query->join('versioncontrol_repositories', 'vr', 'vr.repo_id = vpp.repo_id');
    $query->fields('vr', ['name']);
    $query->join('versioncontrol_gitlab_repositories', 'vgr', 'vr.repo_id = vgr.repo_id');
    $query->fields('vgr', ['gitlab_project_id', 'namespace']);

    $row_data = $query->execute()->fetchAssoc();
    $row->setSourceProperty('drupal_project_nid', $row_data['nid'] ?? '');
    $row->setSourceProperty('gitlab_project_id', $row_data['gitlab_project_id'] ?? '');
    $row->setSourceProperty('gitlab_namespace', $row_data['namespace'] ?? '');
    $row->setSourceProperty('gitlab_project_name', $row_data['name'] ?? '');

    return parent::prepareRow($row);
  }

}
