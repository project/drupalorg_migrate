# DrupalOrg Migrate

Migrations for Drupal 7 (www.drupal.org) to Drupal 10.

Content types, files, users... configuration is already on the site's
`config` folder.

## Initial setup.

Create a database backup of your personal drupal.org copy and bring that copy locally:
```
# SSH to remote.
ssh <your-user>@devwww.drupalsystems.org
cd /var/www/dev/<your-user>-drupal.dev.devdrupal.org/htdocs/
drush sql-dump --result-file --gzip

# Back to your local (ctrl + d).
mkdir -p backups
scp <your-user>@devwww.drupalsystems.org:/home/<your-user>/drush-backups/<full-path-to-file>.sql.gz backups/drupal7.sql.gz
gunzip backups/drupal7.sql.gz
ddev import-db --target-db=migrate <backups/drupal7.sql
gzip backups/drupal7.sql

# SSH to remote, clean-up.
ssh <your-user>@devwww.drupalsystems.org
rm /home/<your-user>/drush-backups/<full-path-to-file>.sql.gz
```

Create `settings.local.php` and include database credentials for the migration database and make
sure the database is there:

```php
// Migrate database.
$databases['migrate']['default'] = array (
  'database' => 'migrate',
  'username' => $databases['default']['default']['username'],
  'password' => $databases['default']['default']['password'],
  'prefix' => '',
  'driver' => 'mysql',
  'charset' => 'utf8mb4',
  'collation' => 'utf8mb4_general_ci',
  'host' => $databases['default']['default']['host'],
  'port' => $databases['default']['default']['port'],
);
```

If you need to adapt the charset of the `migrate` database, please see
[this document](fix_db_collation_commands.md).

A few taxonomy machine names were renamed:
* `vocabulary_1` to `forums`
* `vocabulary_2` to `screenshots`
* `vocabulary_3` to `module_categories`
* `vocabulary_5` to `drupal_version`
* `vocabulary_6` to `core_compatibility`
* `vocabulary_7` to `release_type`
* `vocabulary_31` to `page_status`
* `vocabulary_34` to `front_page_news`
* `vocabulary_38` to `audience`
* `vocabulary_44` to `maintenance_status`
* `vocabulary_46` to `development_status`
* `vocabulary_48` to `services`
* `vocabulary_50` to `sectors`
* `vocabulary_52` to `locations`
* `vocabulary_54` to `keywords`
* `vocabulary_56` to `level`
* `vocabulary_58` to `license`
* `vocabulary_60` to `book_availability`
* `vocabulary_62` to `book_format`

All the fields that started with `taxonomy_vocabulary_` where renamed to `field_` plus the above mapping.

## Migrations

Get a full list of (default) migrations available:
```
ddev drush ms --group=drupalorg_migrate
```

See the file `scripts/migrations.sh` to know all the migrations
and their order.

If you don't follow the order, make sure you append ` --execute-dependencies`.


### Files and media

Copy files (if only subfolders needed then adjust paths):
```
scp -r <your-username>@devwww.drupalsystems.org:/var/www/dev/<your-username>-drupal.dev.devdrupal.org/htdocs/files/project-images web/files/d7-files/files/project-images
```

Migrations to run (included in `<root>/scripts/migrations.sh`:
```
ddev drush mim FILES
```

## Drupal 7 number of users and nodes per content type (2024-10-21)

```
MariaDB [drupal]> select count(*) from user;
+----------------------+
| count(*)             |
+----------------------+
| 2300000+             | ✅ ⚠️ (fields)
+----------------------+

MariaDB [drupal]> select type, count(*) as c from node group by type order by c desc;
+----------------------+---------+
| type                 | c       |
+----------------------+---------+
| project_issue        | 1376660 | ℹ️ (GitLab issues)
| forum                |  366017 | ✅
| project_release      |  236411 | ✅ ⚠️ (fields)
| project_module       |   52484 | ✅️️ ⚠️ (fields)
| book                 |    8621 |
| documentation        |    7788 | ✅ ⚠️ (panels)
| changenotice         |    6088 | ✅
| organization         |    4988 | ✅ ⚠️ (fields)
| project_theme        |    3254 | ✅ ⚠️ (fields)
| casestudy            |    2950 | ✅
| guide                |    1986 | ✅ ⚠️ (panels)
| project_distribution |    1491 | ✅ ⚠️ (fields)
| event                |    1046 | ✅
| post                 |    1020 | ✅ ⚠️ (OG groups)
| page                 |     627 | ✅ ⚠️ (minus panels)
| sa                   |     547 | ✅
| project_drupalorg    |     275 | ✅ ⚠️ (fields)
| project_general      |     149 | ✅ ⚠️ (fields)
| book_listing         |     141 | ✅
| project_core         |     117 | ✅ ⚠️ (fields)
| project_translation  |      99 | ✅ ⚠️ (fields)
| logo                 |      78 | ✅
| section              |      69 |
| contributor_role     |      55 | ✅
| contributor_skill    |      41 | ✅
| contributor_task     |      35 | ✅
| hosting_listing      |      33 | ✅
| project_theme_engine |      20 | ✅ ⚠️ (fields)
+----------------------+---------+
```

- *Update 2024-10-21*: `story` nodes were made `post`. `image` nodes will be removed.

## Drupal 7 field_collections that need migrating to paragraphs

The initial migrations were done but needed to be disabled and the data cleaned up.
See the below `Migration issues` section for more details.

Here is the D7 information:
```
select field_name as type, count(*) as count
from field_collection_item
group by type
order by count desc;
+--------------------------------+--------+
| type                           | count  |
+--------------------------------+--------+
| field_release_files            | 507539 |
| field_organizations            | 370262 |
| field_supporting_organizations |  25191 |
| field_contribution_role        |   4678 |
+--------------------------------+--------+
```

## Migration issues

### 2025-01-23 - Wrong references: disable migrations and remove fields.

We had some issues when migrating field collections to paragraphs, where the references would point to the wrong
paragraph type. This was due (we think) to mapping `revision_id: revision_id` but then not have any mapping to keep
the item IDs being the same.

We needed to remove four fields:
- `field_contribution_role` on the `user` entity.
- `field_organizations` on the `user` entity.
- `field_release_files` on the `project_release` entity.
- `field_supporting_organizations` on the `project_module`, `project_theme`, `project_distribution`, `project_drupalorg`, `project_general`, `project_theme_engine` entities.

These fields will need to be recreated, possibly with a new machine name.

We also stopped running the migrations for those field collections to paragraphs and removed them from the
field mappings in the entities migrations.

### 2025-02-06: New fields and new migrations.

Recreate the four new fields like (note the slightly different machine name):
- `field_user_contribution_role` on the `user` entity.
- `field_user_organizations` on the `user` entity.
- `field_project_release_files` on the `project_release` entity.
- `field_project_supporting_org` on the `project_module`, `project_theme`, `project_distribution`, `project_drupalorg`, `project_general`, `project_theme_engine` entities.

### 2025-02-XX: New migrations for the new fields.

@todo - Redo the migrations to point to the new fields.


## Implemented Hacks

### Missing files

Files are intentionally NOT being migrated right now. The following command was run to disable media migrations:
```
drush pm:uninstall media_migration
```

Additionally, the migrate_devel_file_copy module is installed. This generates file stubs rather than importing real
files. To change this, disable migrate_devel_file_copy and rerun the file migrations.

### Disabled Migrations

#### d7_field_collection and d7_field_collection_revisions

We’ve “cheated” by removing the d7_field_collection and d7_field_collection_revisions migrations for users, thereby
bypassing the problematic d7_field_collection:user:user:field_organizations migration and allowing the other
"User accounts" migrations to continue forward.

However, the d7_field_collection:user:user:field_organizations is largely complete. There are 5 "missing rows."

This was done by applying the following patch to paragraphs:
```
diff --git a/web/modules/contrib/paragraphs/src/Plugin/migrate/D7FieldCollectionItemDeriver.php b/web/modules/contrib/paragraphs/src/Plugin/migrate/D7FieldCollectionItemDeriver.php
index 054c941d..3e945441 100644
--- a/web/modules/contrib/paragraphs/src/Plugin/migrate/D7FieldCollectionItemDeriver.php
+++ b/web/modules/contrib/paragraphs/src/Plugin/migrate/D7FieldCollectionItemDeriver.php
@@ -98,6 +98,8 @@ public function getDerivativeDefinitions($base_plugin_definition) {
         $derivatives = array_reduce($statement->fetchAll(), function (array $carry, $row) {
           $fci_data = unserialize($row->data);
           $carry[$row->entity_type][$row->bundle][$row->field_name] = $fci_data['label'];
+          // @todo Remove this!
+          unset($carry['user']);
           return $carry;
         }, []);
       }
```

#### user

In order to _drastically_ improve the speed of user imports, the following temporary change was made:
```
diff --git a/web/core/lib/Drupal/Core/Validation/Plugin/Validation/Constraint/UniqueFieldValueValidator.php b/web/core/lib/Drupal/Core/Validation/Plugin/Validation/Constraint/UniqueFieldValueValidator.php
index 8adbd57f..2e9353a8 100644
--- a/web/core/lib/Drupal/Core/Validation/Plugin/Validation/Constraint/UniqueFieldValueValidator.php
+++ b/web/core/lib/Drupal/Core/Validation/Plugin/Validation/Constraint/UniqueFieldValueValidator.php
@@ -14,6 +14,9 @@ class UniqueFieldValueValidator extends ConstraintValidator {
    * {@inheritdoc}
    */
   public function validate($items, Constraint $constraint) {
+    // Temp hack.
+    return;
+
     if (!$item = $items->first()) {
       return;
     }
```

This disables the check for user uniqueness and does generate some errors.
