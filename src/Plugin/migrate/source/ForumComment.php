<?php

namespace Drupal\drupalorg_migrate\Plugin\migrate\source;

use Drupal\comment\Plugin\migrate\source\d7\Comment;

/**
 * Drupal 7 Forum comment source from database.
 *
 * @MigrateSource(
 *   id = "d7_comment_forum",
 *   source_module = "comment"
 * )
 */
class ForumComment extends Comment {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = parent::query();
    $query->condition('n.type', 'forum');

    return $query;
  }

}
