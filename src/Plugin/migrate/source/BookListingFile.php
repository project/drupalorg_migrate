<?php

namespace Drupal\drupalorg_migrate\Plugin\migrate\source;

use Drupal\file\Plugin\migrate\source\d7\File;

/**
 * Drupal 7 Book listing images source from database.
 *
 * @MigrateSource(
 *   id = "d7_file_book_listing_files",
 *   source_module = "file"
 * )
 */
class BookListingFile extends File {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = parent::query();
    $query->addJoin('INNER', 'field_data_field_cover_image', 'fci', 'f.fid = %alias.field_cover_image_fid');
    $query->condition('fci.entity_type', 'node');
    $query->condition('fci.bundle', 'book_listing');

    return $query;
  }

}
