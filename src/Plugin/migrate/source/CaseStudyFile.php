<?php

namespace Drupal\drupalorg_migrate\Plugin\migrate\source;

use Drupal\file\Plugin\migrate\source\d7\File;

/**
 * Drupal 7 Case study images source from database.
 *
 * @MigrateSource(
 *   id = "d7_file_casestudy_files",
 *   source_module = "file"
 * )
 */
class CaseStudyFile extends File {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = parent::query();
    // Filter by case study image field.
    if (!isset($this->configuration['source_field'])) {
      $query->addJoin('INNER', 'field_data_upload', 'fdu', 'f.fid = %alias.upload_fid');
      $query->condition('fdu.entity_type', 'node');
      $query->condition('fdu.bundle', 'casestudy');
    }
    elseif ($this->configuration['source_field'] == 'logo') {
      $query->addJoin('INNER', 'field_data_field_logo', 'fl', 'f.fid = %alias.field_logo_fid');
      $query->condition('fl.entity_type', 'node');
      $query->condition('fl.bundle', 'casestudy');
    }
    elseif ($this->configuration['source_field'] == 'mainimage') {
      $query->addJoin('INNER', 'field_data_field_mainimage', 'fmi', 'f.fid = %alias.field_mainimage_fid');
      $query->condition('fmi.entity_type', 'node');
      $query->condition('fmi.bundle', 'casestudy');
    }
    elseif ($this->configuration['source_field'] == 'images') {
      $query->addJoin('INNER', 'field_data_field_images', 'fi', 'f.fid = %alias.field_images_fid');
      $query->condition('fi.entity_type', 'node');
      $query->condition('fi.bundle', 'casestudy');
    }
    elseif ($this->configuration['source_field'] == 'cover_photo') {
      $query->addJoin('INNER', 'field_data_field_cover_photo', 'fcp', 'f.fid = %alias.field_cover_photo_fid');
      $query->condition('fcp.entity_type', 'node');
      $query->condition('fcp.bundle', 'casestudy');
    }

    if (isset($this->configuration['file_type'])) {
      if ($this->configuration['file_type'] == 'image') {
        $query->condition('f.filemime', 'image/%', 'LIKE');
      }
      if ($this->configuration['file_type'] == 'document') {
        $query->condition('f.filemime', 'application/%', 'LIKE');
      }
    }

    return $query;
  }

}
