<?php

namespace Drupal\drupalorg_migrate\Plugin\migrate\source;

use Drupal\file\Plugin\migrate\source\d7\File;

/**
 * Drupal 7 Forum images source from database.
 *
 * @MigrateSource(
 *   id = "d7_file_forum_files",
 *   source_module = "file"
 * )
 */
class ForumFile extends File {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = parent::query();
    $query->addJoin('INNER', 'field_data_upload', 'fdu', 'f.fid = %alias.upload_fid');
    $query->condition('fdu.entity_type', 'node');
    $query->condition('fdu.bundle', 'forum');

    if (isset($this->configuration['file_type'])) {
      if ($this->configuration['file_type'] == 'image') {
        $query->condition('f.filemime', 'image/%', 'LIKE');
      }
      if ($this->configuration['file_type'] == 'document') {
        $query->condition('f.filemime', 'application/%', 'LIKE');
      }
    }

    return $query;
  }

}
